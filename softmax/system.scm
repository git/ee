(define-module (softmax system)
  #:use-module (guix channels)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu packages certs)
  ;; testing:
  #:use-module (gnu packages shells)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages freedesktop)
  ;; 
  #:use-module (gnu packages openbox)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages wm)
  #:use-module (gnu services linux)
  #:use-module (gnu services desktop)
  #:use-module (gnu services networking)
  #:use-module (gnu services security-token)
  #:use-module (gnu services ssh)
  #:use-module (gnu services xorg)
  #:use-module (gnu services base)
  
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd) 

  #:use-module (px system os)
  #:use-module (px system config)
  #:use-module (px packages device)
  #:use-module (px packages monitoring)
  #:use-module (px packages package-management)
  #:use-module (px packages security-token)
  #:use-module (px packages setup)
  #:use-module (px services networking)
  #:use-module (px services device)
  #:use-module (px services log)
  #:use-module (px services monitoring)
  #:use-module (px services package-management)

  #:use-module (softmax packages kyc)
  #:use-module (softmax services kyc)
  #:use-module (softmax packages vhh)
  #:use-module (srfi srfi-1)
  
  #:export (%softmax-kyc-packages
            %softmax-kyc-services
            softmax-kyc-os
            %softmax-general-packages
            %softmax-general-services
            %softmax-workstation-wayland-packages
            %softmax-workstation-wayland-services
            softmax-workstation-wayland-os
            %softmax-workstation-packages
            %softmax-workstation-services
            softmax-workstation-os))


(define %softmax-default-channels
  (append (list (channel
                  (name 'pantherx)
                  (url "https://channels.pantherx.org/git/panther.git")
                  (branch "master")
                  (introduction
                    (make-channel-introduction
                      "54b4056ac571611892c743b65f4c47dc298c49da"
                      (openpgp-fingerprint
                        "A36A D41E ECC7 A871 1003  5D24 524F EB1A 9D33 C9CB"))))
                (channel
                  (name 'ee-softmax)
                  (url "https://channels.ones-now.com/git/ee.git")
                  (branch "master")
                  (introduction
                    (make-channel-introduction
                      "7f8aa031813c671669e9e63f174174de2a74a9c3"
                      (openpgp-fingerprint
                        "A36A D41E ECC7 A871 1003  5D24 524F EB1A 9D33 C9CB"))))
                (channel
                  (name 'nonguix)
                  (url "https://channels.pantherx.org/git/nonguix.git")
                  (branch "master")
                  (introduction
                    (make-channel-introduction
                      "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                      (openpgp-fingerprint
                        "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5")))))
          %default-channels))


;;;
;;; Utility functions
;;;
(define (running-tty tty)
  (service mingetty-service-type (mingetty-configuration
                                  (tty tty))))

;;;
;;; General Configuration Packages
;;;
(define %softmax-general-packages
  (append
   (list
    ;; openbox
    ;; TODO: Remove
    xterm
    px
    px-device-identity
    px-device-runner
    px-org-remote-status-service
    px-setup-assistant)
   %px-core-packages))

;;;
;;; General Configuration Services
;;;
(define %softmax-general-services
  (cons*
   (service dhcp-client-service-type)
   
   (service openssh-service-type
      (openssh-configuration
             (port-number 22)
       (permit-root-login #t)))
   
   (service elogind-service-type)
   (service earlyoom-service-type)
   (service nftables-service-type) ;; firewall; blocks all except SSH
   
   (service px-device-identity-service-type)
   
   (service px-remote-status-service-type
            (px-remote-status-service-configuration
             (interval 300)))
   
   ;; Replace with guix unattended-upgrades
   (service px-unattended-upgrades-service-type
            (px-unattended-upgrades-configuration
             (timeout 120)))
   
   (service px-device-runner-service-type
      (px-device-runner-configuration
       (schedule "*/3 * * * *")))
   
   (modify-services %base-services
                    (delete mingetty-service-type)
                    (guix-service-type
                      config => (guix-configuration
                                (inherit config)
                                ;; not yet available at this commit
                                ;; (guix (guix-for-channels %softmax-default-channels))
                                (authorized-keys
                                  (cons* %px-substitute-server-key
                                        %nonguix-substitute-server-key
                                        %default-authorized-guix-keys))
                                (substitute-urls
                                  (cons* %px-substitute-server-url
                                        %nonguix-substitute-server-url
                                        %default-substitute-urls))
                                (channels %softmax-default-channels))))))

;;;
;;; KYC Packages
;;;
(define %softmax-kyc-packages
  (append
   (list kyc
         openbox)
   %softmax-general-packages))

;;;
;;; KYC Services
;;;
(define %softmax-kyc-services
  (cons*
   
   (running-tty "tty1")
   (running-tty "tty2")
   (running-tty "tty3")
   (running-tty "tty4")
   (running-tty "tty5")
   (running-tty "tty6")
   
   ;;; KYC as default environment                                                                                                                                                                                                    
   (service slim-service-type
            (slim-configuration
             (xorg-configuration
              (xorg-configuration
               (extra-config `("Section \"ServerFlags\"\n"
                               "   Option \"BlankTime\" \"0\"\n"
                               "   Option \"StandbyTime\" \"0\"\n"
                               "   Option \"SuspendTime\" \"0\"\n"
                               "   Option \"OffTime\" \"0\"\n"
                               "EndSection\n"
                               "\n"))))
             (vt "vt7")
             (auto-login? #t)
             (default-user "kyc-user")
             (auto-login-session
              (file-append openbox "/bin/openbox --startup \"kyc-gui\""))))
   
   ;;; KYC Service                                                                                                                                                                                                                   
   (service kyc-service-type)
   
   ;;; Card reader service                                                                                                                                                                                                          
   (service pcscd-service-type
            (pcscd-configuration
             (usb-drivers (list acsccid))))
   
   ;;; Upload enrollment recordings to IDP                                                                                                                                                                                           
   (service px-file-upload-service-type
            (px-file-upload-configuration
             (schedule "*/5 * * * *")
             (types '("mp4"))
             (source "/root/.local/share/kyc-service/Recording_enrollment/")
             (endpoint "/users/registration/{userid}/video")
             (keys '("{userid:uuid}"))
             (parse? #t)
             (delete-on-success? #t)))
   
   ;;; Improve time keeping                                                                                                                                                                                                          
   (service chrony-service-type
            (chrony-service-configuration
             (user "kyc-user")))
   
   %softmax-general-services))

;;;
;;; KYC Configuration
;;; System configuration specifically for KYC
;;;
(define (softmax-kyc-os config)
  (let ((target-packages (prepare-packages config %softmax-kyc-packages))
        (target-services (prepare-services config %softmax-kyc-services))
        (target-bootloader (adjust-bootloader-theme config)))
    (operating-system
     (inherit config)
     
     (kernel linux)
     (initrd microcode-initrd)
     (firmware (list linux-firmware))
     (bootloader target-bootloader)
     (swap-devices '("/swapfile"))
     
     (groups (cons* (user-group (name "kyc-rpc"))
                    (user-group (name "kyc-user"))
                    %base-groups))
     
     (users (cons* (user-account
                    (name "kyc-user")
                    (group "kyc-user")
                    (supplementary-groups '("users" "kyc-rpc"))
                    (comment "kyc user account"))
                   (user-account
                    (name "kyc-service")
                    (group "kyc-service")
                    (supplementary-groups '("users" "wheel" "kyc-rpc"))
                    (comment "kyc service account"))
                   %base-user-accounts))
     
     (packages target-packages)
     (services target-services)
     (name-service-switch %mdns-host-lookup-nss))))

;;;                                                                                                                                                                                                                                            
;;; General Configuration
;;; System configuration for Workstation and others
;;;

(define %softmax-workstation-wayland-packages
  (cons* waybar
         qtwayland-5
         labwc
         vhh-workstation
         vhh-workstation-autostart-wayland-script
         %softmax-general-packages))


(define %workstation-motd
  (plain-file "motd" "VHH Workstation: Welcome.\n\n"))

(define %softmax-workstation-wayland-services
  (cons*
   (service greetd-service-type
      (greetd-configuration
       (motd %workstation-motd)
       (greeter-supplementary-groups (list "video" "input"))
       (terminals
              (list
               (greetd-terminal-configuration 
                (terminal-vt "1")
                (terminal-switch #t)
                (default-session-command
                  (greetd-agreety-session 
                  (command (file-append labwc "/bin/labwc"))
                  (command-args '("-s vhh-workstation.sh")))))
              (greetd-terminal-configuration
                (terminal-vt "2"))
              (greetd-terminal-configuration
                (terminal-vt "3"))
              (greetd-terminal-configuration
                (terminal-vt "4"))
              (greetd-terminal-configuration
                (terminal-vt "5"))
              (greetd-terminal-configuration
                (terminal-vt "6"))))))
   
    ;;; Improve time keeping
   (service chrony-service-type
      (chrony-service-configuration
             (user "softmax")))
   
   %softmax-general-services))


(define (softmax-workstation-wayland-os config)
  (let ((target-packages (prepare-packages config %softmax-workstation-wayland-packages))
        (target-services (prepare-services config %softmax-workstation-wayland-services))
        (target-bootloader (adjust-bootloader-theme config)))
    (operating-system
     (inherit config)
     
     ;; linux-px for LTS
     (kernel linux)
     (issue
      "\
VHH Workstation (WAYLAND)\n
Login with 'softmax' to start.\n
\n")
     (initrd microcode-initrd)
     (firmware (list linux-firmware))
     (bootloader target-bootloader)
     (swap-devices '("/swapfile"))
     
     (users
      (cons*
       (user-account
        (name "softmax")
        (group "users")
        (supplementary-groups '("audio" "video"))
        (password "")
        (comment "general user account"))
       %base-user-accounts))
     
     (packages target-packages)
     (services target-services)
     (name-service-switch %mdns-host-lookup-nss))))



(define %softmax-workstation-packages
  (cons* vhh-workstation-autostart-script
         %softmax-general-packages))

(define %softmax-workstation-services
  (cons*

   (running-tty "tty1")
   (running-tty "tty2")
   (running-tty "tty3")
   (running-tty "tty4")
   (running-tty "tty5")
   (running-tty "tty6")

   ;; VHH-Workstation as default environment
   (service slim-service-type
      (slim-configuration
             (xorg-configuration
              (xorg-configuration
               (extra-config `("Section \"ServerFlags\"\n"
                               "   Option \"BlankTime\" \"0\"\n"
                               "   Option \"StandbyTime\" \"0\"\n"
                               "   Option \"SuspendTime\" \"0\"\n"
                               "   Option \"OffTime\" \"0\"\n"
                               "EndSection\n"
                               "\n"))))
             (vt "vt7")
             (auto-login? #t)
             (default-user "softmax")
             (auto-login-session
              (file-append openbox "/bin/openbox-session"))))
   
    ;;; Improve time keeping
   (service chrony-service-type
      (chrony-service-configuration
             (user "softmax")))
   
   %softmax-general-services))


(define (softmax-workstation-os config)
  (let ((target-packages (prepare-packages config %softmax-workstation-packages))
        (target-services (prepare-services config %softmax-workstation-services))
        (target-bootloader (adjust-bootloader-theme config)))
    (operating-system
     (inherit config)
     
     ;; linux-px for LTS
     (kernel linux)
     (issue
      "\
VHH Workstation (X11)
Login with 'softmax' to start.\n
\n")
     (initrd microcode-initrd)
     (firmware (list linux-firmware))
     (bootloader target-bootloader)
     (swap-devices '("/swapfile"))
     
     (users
      (cons*
       (user-account
        (name "softmax")
        (group "users")
        (supplementary-groups '("audio" "video"))
        (password "")
        (comment "general user account"))
       %base-user-accounts))
     
     (packages target-packages)
     (services target-services)
     (name-service-switch %mdns-host-lookup-nss))))
