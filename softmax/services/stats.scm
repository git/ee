(define-module (softmax services stats)
  #:use-module (gnu)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages video)
  #:use-module (gnu services mcron)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system)
  #:use-module (gnu system shadow)
  #:use-module (px packages device)
  #:use-module (px packages security-token)
  #:use-module (px packages tpm)
  #:use-module (softmax packages crates)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  
  #:export (<ones-system-stats-configuration>
            ones-system-stats-configuration
            ones-system-stats-configuration?
            ones-system-stats-type))

;;
;; Stats
;;

(define-record-type* <ones-system-stats-configuration>
  ones-system-stats-configuration make-ones-system-stats-configuration
  ones-system-stats-configuration?
  (package ones-system-stats-configuration-package
           (default ones-system-stats)))

(define (ones-system-stats-shepherd-service config)
  (match config
    (($ <ones-system-stats-configuration> package)
     (list (shepherd-service
            (provision '(ones-system-stats))
            (documentation "Run ones-system-stats as a shepherd daemon")
            (requirement `(networking user-processes))
            (start #~(make-forkexec-constructor
                      (list (string-append #$screen "/bin/screen")
                            "-D" "-m" "-S" "bluetooth-terminal-alerts"
                            (string-append #$package "/bin/ones-system-stats"))
                      #:environment-variables
                      (cons* "HOME=/root"
                             "XDG_DATA_HOME=/root/.local/share"
                             "XDG_CONFIG_HOME=/root/.config"
                             "SSL_CERT_DIR=/run/current-system/profile/etc/ssl/certs"
                             "SSL_CERT_FILE=/run/current-system/profile/etc/ssl/certs/ca-certificates.crt"
                             (default-environment-variables))))
            (stop #~(make-kill-destructor)))))))

(define ones-system-stats-type
  (service-type
   (name 'ones-system-stats)
   (description "Runs ones-system-stats as service.")
   (extensions (list (service-extension shepherd-root-service-type
                                        ones-system-stats-shepherd-service)))
   (default-value (ones-system-stats-configuration))))
