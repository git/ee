;;; Copyright © 2020 Reza Alizadeh Majd <r.majd@pantherx.org> | PantherX.DEV

(define-module (softmax services kyc)
  #:use-module (gnu)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages video)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system)
  #:use-module (gnu system shadow)

  #:use-module (px packages device)
  #:use-module (softmax packages kyc)
  #:use-module (px packages security-token)
  #:use-module (px services log)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)

  #:export (
            <kyc-configuration>
            kyc-configuration
            kyc-configuration?
            kyc-service-type))


(define-record-type* <kyc-configuration>
  kyc-configuration make-kyc-configuration
  kyc-configuration?
  (package kyc-configuration-package
           (default kyc))
  (user kyc-configuration-user
        (default "kyc-service"))
  (group kyc-configuration-group
         (default "kyc-service"))
  (extra-options kyc-configuration-options
                 (default '())))


(define %kyc-accounts
  (list (user-group (name "kyc-service") (system? #t))
        (user-account
          (name "kyc-service")
          (group "kyc-service")
          (system? #t)
          (comment "KYC service user")
          (home-directory "/var/empty")
          (shell (file-append shadow "/sbin/nologin")))))


(define kyc-shepherd-service
  (match-lambda
    (($ <kyc-configuration> package user group extra-options)
      (list (shepherd-service
              (provision '(kyc))
              (documentation "Run kyc-service as shepherd service daemon.")
              (requirement '(px-device-identity networking user-processes))
              (modules `((srfi srfi-1)
                         (srfi srfi-26)
                         ,@%default-modules))
              (start #~(make-forkexec-constructor
                        (list
                          (string-append #$screen "/bin/screen")
                          "-D" "-m" "-S" "kyc-service"
                          (string-append #$package "/bin/kyc-service")
                          #$@extra-options)
                        ;; TODO: keep user and roup modification commented until we find a solution
                        ;;       for granting alternative permissions to `kyc-service` user
                        ; #:user #$user
                        ; #:group #$group
                        #:environment-variables
                          (cons*  (string-append "PATH="
                                                 #$id-card-reader "/bin:"
                                                 #$coreutils "/bin:"
                                                 #$ffmpeg "/bin:"
                                                 #$procps "/bin:"
                                                 "/run/current-system/profile/bin:"
                                                 (getenv "PATH"))
                                  "HOME=/root"
                                  "XDG_DATA_HOME=/root/.local/share"
                                  "XDG_CONFIG_HOME=/root/.config"
                                  "SSL_CERT_DIR=/run/current-system/profile/etc/ssl/certs"
                                  "SSL_CERT_FILE=/run/current-system/profile/etc/ssl/certs/ca-certificates.crt"
                                  (remove (cut string-prefix? "PATH=" <>)
                                            (environ)))
                        ))
              (stop #~(make-kill-destructor)))))))


(define kyc-service-type
  (service-type
    (name 'kyc)
    (description "Ones KYC service")
    (extensions (list (service-extension shepherd-root-service-type
                                         kyc-shepherd-service)
                      (service-extension account-service-type
                                         (const %kyc-accounts))))
    (default-value (kyc-configuration))))