(define-module (softmax services alerts)
  #:use-module (gnu)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages video)
  #:use-module (gnu services mcron)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system)
  #:use-module (gnu system shadow)
  #:use-module (px packages device)
  #:use-module (px packages security-token)
  #:use-module (px packages tpm)
  #:use-module (softmax packages alerts)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  
  #:export (<vhh-bluetooth-terminal-assigned-user-configuration>
            vhh-bluetooth-terminal-assigned-user-configuration
            vhh-bluetooth-terminal-assigned-user-configuration?
            vhh-bluetooth-terminal-assigned-user-type
	    
            <vhh-bluetooth-terminal-alerts-configuration>
            vhh-bluetooth-terminal-alerts-configuration
            vhh-bluetooth-terminal-alerts-configuration?
            vhh-bluetooth-terminal-alerts-type))

;;
;; Assigned User
;;

(define-record-type* <vhh-bluetooth-terminal-assigned-user-configuration>
  vhh-bluetooth-terminal-assigned-user-configuration make-vhh-bluetooth-terminal-assigned-user-configuration
  vhh-bluetooth-terminal-assigned-user-configuration?
  (package vhh-bluetooth-terminal-assigned-user-configuration-package
           (default vhh-bluetooth-terminal-assigned-user)))

(define (vhh-bluetooth-terminal-assigned-user-shepherd-service config)
  (match config
    (($ <vhh-bluetooth-terminal-assigned-user-configuration> package)
     (list (shepherd-service
            (provision '(vhh-bluetooth-terminal-assigned-user))
            (documentation "Run vhh-bluetooth-terminal-assigned-user as a shepherd daemon")
            (requirement `(networking user-processes))
            (start #~(make-forkexec-constructor
                      (list (string-append #$screen "/bin/screen")
                            "-D" "-m" "-S" "bluetooth-terminal-assigned-user"
                            (string-append #$package "/bin/vhh-bluetooth-terminal-assigned-user"))
                      #:environment-variables
                      (cons* "HOME=/root"
                             "XDG_DATA_HOME=/root/.local/share"
                             "XDG_CONFIG_HOME=/root/.config"
                             "SSL_CERT_DIR=/run/current-system/profile/etc/ssl/certs"
                             "SSL_CERT_FILE=/run/current-system/profile/etc/ssl/certs/ca-certificates.crt"
                             (default-environment-variables))))
            (stop #~(make-kill-destructor)))))))

(define vhh-bluetooth-terminal-assigned-user-type
  (service-type
   (name 'vhh-bluetooth-terminal-assigned-user)
   (description "Runs vhh-bluetooth-terminal-assigned-user as service.")
   (extensions (list (service-extension shepherd-root-service-type
                                        vhh-bluetooth-terminal-assigned-user-shepherd-service)))
   (default-value (vhh-bluetooth-terminal-assigned-user-configuration))))

;;
;; Alerts
;;

(define-record-type* <vhh-bluetooth-terminal-alerts-configuration>
  vhh-bluetooth-terminal-alerts-configuration make-vhh-bluetooth-terminal-alerts-configuration
  vhh-bluetooth-terminal-alerts-configuration?
  (package vhh-bluetooth-terminal-alerts-configuration-package
           (default vhh-bluetooth-terminal-alerts)))

(define (vhh-bluetooth-terminal-alerts-shepherd-service config)
  (match config
    (($ <vhh-bluetooth-terminal-alerts-configuration> package)
     (list (shepherd-service
            (provision '(vhh-bluetooth-terminal-alerts))
            (documentation "Run vhh-bluetooth-terminal-alerts as a shepherd daemon")
            (requirement `(networking user-processes))
            (start #~(make-forkexec-constructor
                      (list (string-append #$screen "/bin/screen")
                            "-D" "-m" "-S" "bluetooth-terminal-alerts"
                            (string-append #$package "/bin/vhh-bluetooth-terminal-alerts"))
                      #:environment-variables
                      (cons* "HOME=/root"
                             "XDG_DATA_HOME=/root/.local/share"
                             "XDG_CONFIG_HOME=/root/.config"
                             "SSL_CERT_DIR=/run/current-system/profile/etc/ssl/certs"
                             "SSL_CERT_FILE=/run/current-system/profile/etc/ssl/certs/ca-certificates.crt"
                             (default-environment-variables))))
            (stop #~(make-kill-destructor)))))))

(define vhh-bluetooth-terminal-alerts-type
  (service-type
   (name 'vhh-bluetooth-terminal-alerts)
   (description "Runs vhh-bluetooth-terminal-alerts as service.")
   (extensions (list (service-extension shepherd-root-service-type
                                        vhh-bluetooth-terminal-alerts-shepherd-service)))
   (default-value (vhh-bluetooth-terminal-alerts-configuration))))
