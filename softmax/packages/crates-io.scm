(define-module (softmax packages crates-io)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages crates-io)
  #:use-module (srfi srfi-1))

(define-public rust-assert-hex-0.4
  (package
    (name "rust-assert-hex")
    (version "0.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "assert_hex" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "19xwlhz2swak1gw6rv38wai7d1xn9l3mspnv1fl8rz7h60a0y0gp"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/wcampbell0x2a/assert_hex")
    (synopsis "Display assert panics in hexadecimal format")
    (description
     "Rust library to display assert panics in hexadecimal format")
    (license license:expat)))

(define-public rust-libudev-sys-0.1
  (package
    (name "rust-libudev-sys")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libudev-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "09236fdzlx9l0dlrsc6xx21v5x8flpfm3d5rjq9jr5ivlas6k11w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-libc" ,rust-libc-0.2))
       #:cargo-development-inputs
       (("rust-pkg-config" ,rust-pkg-config-0.3))))
    (native-inputs
     (list pkg-config
           eudev))
    (home-page "https://github.com/dcuddeback/libudev-rs")
    (synopsis "Rust wrapper for libudev")
    (description
     "This crate provides a safe wrapper around the native libudev library.")
    (license license:expat)))

(define-public rust-libudev-0.3
  (package
    (name "rust-libudev")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libudev" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1q1my5alvdwyi8i9pc9gn2mcx5rhbsssmz5cjnxzfpd65laj9cvq"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           (substitute* "Cargo.toml"
             (("0.1.3") "0.1.4"))))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-libc" ,rust-libc-0.2)
        ("rust-libudev-sys" ,rust-libudev-sys-0.1))
       #:cargo-development-inputs
        (("rust-pkg-config" ,rust-pkg-config-0.3))))
    (native-inputs
     (list pkg-config
           eudev))
    (home-page "https://github.com/dcuddeback/libudev-rs")
    (synopsis "Rust wrapper for libudev")
    (description
     "This crate provides a safe wrapper around the native libudev library.")
    (license license:expat)))

(define-public rust-unescaper-0.1
  (package
    (name "rust-unescaper")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unescaper" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0d4xi16mindhksi3lqvn0kzzgj5az9qbgxqmz7gwmcxm5v9nmpqa"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/hack-ink/unescaper")
    (synopsis "Unescape strings with escape sequences.")
    (description
     "Unescape strings with escape sequences written out as literal characters.")
    (license license:expat)))

(define-public rust-serialport-4
  (package
    (name "rust-serialport")
    (version "4.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "serialport" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0a380655dqrl16wfqnr75zxk3f8h3fshjjr68r4gh3clpv81anlg"))
       (patches (search-patches "rust-serialport-remove-macos.patch"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-bitflags" ,rust-bitflags-2)
        ("rust-cfg-if" ,rust-cfg-if-1)
        ("rust-libudev" ,rust-libudev-0.3)
        ("rust-nix" ,rust-nix-0.26)
        ("rust-regex" ,rust-regex-1)
        ("rust-scopeguard" ,rust-scopeguard-1)
        ("rust-serde" ,rust-serde-1)
        ("rust-unescaper" ,rust-unescaper-0.1))
       #:cargo-development-inputs (("rust-assert-hex" ,rust-assert-hex-0.4)
                                   ("rust-clap" ,rust-clap-3))))
    (native-inputs
     (list pkg-config
           eudev))
    (home-page "https://github.com/serialport/serialport-rs")
    (synopsis "A cross-platform serial port library in Rust.")
    (description
     "A cross-platform serial port library in Rust. Provides a blocking I/O
interface and port enumeration including USB device information.")
    (license license:expat)))

(define-public rust-clap-builder-4
  (package
    (name "rust-clap-builder")
    (version "4.5.27")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "clap_builder" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1mys7v60lys8zkwpk49wif9qnja9zamm4dnrsbj40wdmni78h9hv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-anstream" ,rust-anstream-0.6)
                       ("rust-anstyle" ,rust-anstyle-1)
                       ("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-clap-lex" ,rust-clap-lex-0.7)
                       ("rust-strsim" ,rust-strsim-0.11)
                       ("rust-terminal-size" ,rust-terminal-size-0.4)
                       ("rust-unicase" ,rust-unicase-2)
                       ("rust-unicode-width" ,rust-unicode-width-0.2))
       #:cargo-development-inputs (("rust-color-print" ,rust-color-print-0.3)
                                   ("rust-static-assertions" ,rust-static-assertions-1)
                                   ("rust-unic-emoji-char" ,rust-unic-emoji-char-0.9))))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis
     "simple to use, efficient, and full-featured Command Line Argument Parser")
    (description
     "This package provides a simple to use, efficient, and full-featured Command Line
Argument Parser.")
    (license (list license:expat license:asl2.0))))

(define-public rust-clap-derive-4
  (package
    (name "rust-clap-derive")
    (version "4.5.24")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "clap_derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "131ih3dm76srkbpfx7zfspp9b556zgzj31wqhl0ji2b39lcmbdsl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-heck" ,rust-heck-0.5)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis "Parse command line argument by defining a struct, derive crate")
    (description
     "This package provides Parse command line argument by defining a struct, derive crate.")
    (license (list license:expat license:asl2.0))))