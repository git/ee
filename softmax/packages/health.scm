(define-module (softmax packages health)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages crates-io)
  #:use-module (softmax packages crates-io)
  #:use-module (srfi srfi-1))

(define-public usb-bpm-exporter
  (package
    (name "usb-bpm-exporter")
    (version "0.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
       (sha256
        (base32 "0a1pp0dfrh9lwjs0k3sdglasy3jvnqg4j5386yfv6wa8iag1ikjp"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           (substitute* "Cargo.toml"
             (("0.4.37") "0.4.34"))))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("serialport" ,rust-serialport-4)
        ("rust-byteorder" ,rust-byteorder-1)
        ("rust-chrono" ,rust-chrono-0.4)
        ("rust-csv" ,rust-csv-1))))
    (native-inputs
     (list pkg-config
           eudev))
    (home-page "https://pantherx.org")
    (synopsis "A simple logging library for Rust")
    (description "A simple logging library for Rust")
    (license license:expat)))