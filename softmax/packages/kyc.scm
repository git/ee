(define-module (softmax packages kyc)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system qt)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages security-token)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (gnu packages machine-learning)
  #:use-module (px packages monitoring)
  #:use-module (px packages security-token)
  #:use-module ((guix licenses) #:prefix license:))

(define-public kyc
  (package
   (name "kyc")
   (version "1.5.6")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://source.pantherx.org/project-2.15-desktop-kyc" 
                         "_v" version ".tgz"))
     (sha256 (base32 "1s8kvsds46rphiml1chnmvx7klmypnlhlp2nbsx25gcjlc5fs1gd"))))
   (build-system qt-build-system)
   (arguments `(#:tests? #f))
   (propagated-inputs
    `(("id-card-reader" ,id-card-reader)
      ("ffmpeg" ,ffmpeg)
      ("gst-plugins-bad" ,gst-plugins-bad)
      ("gst-plugins-good" ,gst-plugins-good)
      ("gst-libav" ,gst-libav)
      ("qtmultimedia" ,qtmultimedia-5)))
   (native-inputs
    `(("qrcodegen-cpp" ,qrcodegen-cpp)
      ("capnproto" ,capnproto)
      ("qtbase" ,qtbase-5)
      ("qtsvg" ,qtsvg-5)
      ("pkg-config" ,pkg-config)
      ("dlib" ,dlib)
      ("giflib" ,giflib)
      ("lapack" ,lapack)
      ("libjpeg" ,libjpeg-turbo)
      ("libpng" ,libpng)
      ("sentry" ,sentry-native-0.6)
      ("v4l2loopback-linux-module" ,v4l2loopback-linux-module)
      ("v4l-utils" ,v4l-utils)
      ("openblas" ,openblas)
      ("zlib" ,zlib)))
   (home-page "https://www.pantherx.dev")
   (synopsis "Enterprise KYC enrollment front-end AND back-end")
   (description "This package provides the QT user enrollment front-end and kyc-service as back-end.")
   (license license:expat)))