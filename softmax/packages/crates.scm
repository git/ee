(define-module (softmax packages crates)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-web)
  #:use-module (px packages crates-io)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public rust-ones-oidc
  (package
    (name "rust-ones-oidc")
    (version "0.2.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ones-oidc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1yzgs1dfl0rx5x8zngsp0x9rn2f663ifivb5psgrvzlvayq2arxc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-tokio", rust-tokio-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-yml" ,rust-serde-yml-0.0.12)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-reqwest" ,rust-reqwest-0.11)
                       ("rust-openidconnect" ,rust-openidconnect-3)
                       ("rust-jsonwebtoken" ,rust-jsonwebtoken-9)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-simplelog" ,rust-simplelog-0.12)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-thiserror" ,rust-thiserror-2))))
    (inputs `(("openssl" ,openssl)))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (home-page "https://www.softmax.co.th")
    (synopsis "ONES OIDC Library")
    (description "Device, CIBA, QR and Matrix authentication library")
    (license license:expat)))

(define-public ones-system-stats
  (package
    (name "ones-system-stats")
    (version "0.1.6")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://source.pantherx.org/" name "-" version
                           ".crate"))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0lr927x89fwqzmj4cwvf44m6242fyx9zxpl7scn16khxrmilcijc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs (("rust-tokio", rust-tokio-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-reqwest" ,rust-reqwest-0.11)
                       ("rust-openidconnect" ,rust-openidconnect-3)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-simplelog" ,rust-simplelog-0.12)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-dotenvy" ,rust-dotenvy-0.15)
                       ("rust-sysinfo" ,rust-sysinfo-0.32)
                       ("rust-anyhow" ,rust-anyhow-1)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-ones-oidc" ,rust-ones-oidc))))
    (inputs `(("openssl" ,openssl)))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (home-page "https://www.softmax.co.th")
    (synopsis "ONES OIDC Library")
    (description "Device, CIBA, QR and Matrix authentication library")
    (license license:expat)))