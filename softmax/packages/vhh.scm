(define-module (softmax packages vhh)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (ice-9 match)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages openbox)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages unicode)
  #:use-module (px packages gstreamer)
  #:use-module (px packages library)
  #:use-module (px packages matrix-client)
  #:use-module (px packages themes)
  #:use-module (px packages monitoring)
  #:use-module (softmax packages cpp)
  #:use-module (softmax packages matrix)
  #:use-module (srfi srfi-1)
  #:use-module ((guix licenses) #:prefix license:))

(define-public vhh-qml-components
  (package
    (name "vhh-qml-components")
    (version "0.0.25")
    (source
    (origin
      (method url-fetch)
      (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
      (sha256
       (base32 "0jqmlj1s66kcq9dyvrz6yqbrflmqysfk6c6ab9v1nvbpc70sxvbd"))))
    (build-system qt-build-system)
    (arguments
     `(#:tests? #f))
    (inputs (list qtbase-5
                  qtcharts-5
                  qtdeclarative-5
                  qtquickcontrols-5
                  qtquickcontrols2-5
                  qtgraphicaleffects
                  qtmultimedia-5
                  qtlocation-5
                  qtsvg-5
                  ones-stats-cpp))
    (native-inputs (list pkg-config))
   (home-page "https://www.softmax.co.th")
   (synopsis "QML components for VHH Applications")
   (description "QML components shared across VHH applications
like KYC, Workstation and so on.")
    (license license:expat)))

(define-public vhh-workstation
  (package
   (name "vhh-workstation")
   (version "0.0.72")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
     (sha256 (base32 "1dr8kshwl0fj6n5b75nyi468wxqp2cxzdsa44w2ki92b81m37lx0"))))
   (build-system qt-build-system)
   (arguments
    `(#:tests? #f
      #:phases
      (modify-phases
       %standard-phases
       (add-after 'install 'wrap-to-patch-scripts-requirements
		  (lambda* (#:key outputs inputs #:allow-other-keys)
		    (let ((out         (assoc-ref outputs "out"))
			  (coreutils   (assoc-ref inputs "coreutils"))
			  (util-linux  (assoc-ref inputs "util-linux"))
			  (inetutils   (assoc-ref inputs "inetutils"))
			  (xrandr      (assoc-ref inputs "xrandr")))
		      (wrap-program (string-append out "/bin/vhh-init-monitors.sh") 
				    `("PATH" ":" prefix (,(string-append xrandr "/bin"))))
		      (wrap-program (string-append out "/bin/vhh-init-monitors.sh") 
				    `("PATH" ":" prefix (,(string-append inetutils "/bin"))))
		      (wrap-program (string-append out "/bin/vhh-init-monitors.sh") 
				    `("PATH" ":" prefix (,(string-append util-linux "/bin"))))
		      (wrap-program (string-append out "/bin/vhh-init-monitors.sh") 
				    `("PATH" ":" prefix (,(string-append coreutils "/bin"))))    
		      (wrap-program (string-append out "/bin/vhh-workstation.sh") 
				    `("PATH" ":" prefix (,(string-append out "/bin"))))
		      (invoke "chmod" "--recursive" "+x" (string-append out "/bin/"))
		      #t))))))
   (inputs
    (list coeurl
          curl
          json-modern-cxx
          libevent
          libolm
          lmdb
          lmdbxx
          matrix-client-library-with-ciba-v2
          matrix-client-gui-library-v2
          ones-oidc-cpp
          ones-stats-cpp
          qmtxclient
          openssl
          qrcodegen-cpp
          qtbase-5
          qtcharts-5
          qtdeclarative-5
          qtgraphicaleffects
          qtlocation-5
          qtmultimedia-5
          qtquickcontrols-5
          qtquickcontrols2-5
          qtsvg-5
          px-auth-library-cpp
          px-widget-style
          spdlog
          xcb-util-wm
          zlib
          coreutils
          inetutils
          util-linux
          xrandr
          vhh-qml-components
          unicode-emoji
          font-ibm-plex
          jq))
   (propagated-inputs
    (list            
     gst-plugins-good-qmlgl       ;; rtpmanager for voip
     gst-plugins-base
     gst-plugins-bad              ;; sdp & webrtc for voip
     libnice))                    ;; for voip
   
   (native-inputs
    `(("pkg-config" ,pkg-config)
      ("sentry" ,sentry-native-0.6)))
   (home-page "https://www.pantherx.dev")
   (synopsis "Supervisor Workstation and Monitoring N* Workstations")
   (description "Supervisor Workstation and Monitoring N* Workstations")
   (license license:expat)))

; (define-public vhh-workstation-v2
;   (package
;     (inherit vhh-workstation)
;     (name "vhh-workstation-v2")
;     (version "0.0.71")
;     (source
;      (origin
;       (method url-fetch)
;       (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
;       (sha256 (base32 "07y77jz803qqdz58sqqg4ziq34passj04qmcwxckn7d3jrd7pck1"))))
;     (inputs `(("qrcodegen-cpp" ,qrcodegen-cpp)
;               ("ones-oidc-cpp" ,ones-oidc-cpp)
;               ("matrix-client-library-with-ciba" ,matrix-client-library-with-ciba-v2)
;               ("matrix-client-gui-library" ,matrix-client-gui-library-v2)
;               ,@(fold alist-delete
;                       (package-inputs vhh-workstation)
;                       '("matrix-client-library-with-ciba" "matrix-client-gui-library"))))))

(define-public openbox-vhh
  (package
    (inherit openbox)
    (name "openbox")
    (build-system gnu-build-system)
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'replace-config
            (lambda _
              (delete-file "configure")
              (delete-file "data/rc.xml")
              (call-with-output-file "data/rc.xml"
                (lambda (port)
                  (format port "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<openbox_config xmlns=\"http://openbox.org/3.4/rc\" xmlns:xi=\"http://www.w3.org/2001/XInclude\">
  <applications>
    <application class=\"*\">
      <decor>no</decor>
      <maximized>true</maximized>
    </application>
  </applications>
</openbox_config>"))))))))))

(define-public vhh-workstation-autostart-script
  (package
   (name "vhh-workstation-autostart-script")
   (version "0.0.4")
   (source #f)
   (build-system trivial-build-system)
   (inputs `(("coreutils" ,coreutils)))
   (propagated-inputs `(("vhh-workstation" ,vhh-workstation)
                        ("openbox" ,openbox-vhh)
                        ;; ("polybar" ,polybar)
                        ;; ("xterm" ,xterm)
                        ("python", python)
                        ("python-pyxdg", python-pyxdg)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out           (assoc-ref %outputs "out"))
               (autostart-dir (string-append out "/etc/xdg/autostart"))
               (executable    (string-append (assoc-ref %build-inputs "vhh-workstation")
                                             "/bin/vhh-workstation.sh"))
               (desktop-entry (string-append autostart-dir "/vhh-workstation-start.desktop")))
          (setenv "PATH" 
		  (string-append (string-append (assoc-ref %build-inputs "coreutils")  "/bin:")
				 (getenv "PATH")))
          ;; Adjust the autostart entry
          (mkdir-p autostart-dir)
          (call-with-output-file desktop-entry
            (lambda (port)
              (format port "[Desktop Entry]
Type=Application
Name=Launcher
Exec='~a' -style Breeze
Comment=Launcher to configure networking and start Matrix Client
X-GNOME-Autostart-enabled=true"
                      executable)))
          (chmod desktop-entry #o555)

          ;; Polybar config.ini
          (let* ((rc-dir  (string-append out "/etc/xdg/polybar"))
                 (rc-file (string-append rc-dir "/config.ini")))
            (mkdir-p (string-append out "/etc/xdg/polybar"))
            (call-with-output-file (string-append out "/etc/xdg/polybar/config.ini")
              (lambda (port)
                (format port "[colors]
background = #282A2E
background-alt = #373B41
foreground = #C5C8C6
primary = #F0C674
secondary = #8ABEB7
alert = #A54242
disabled = #707880

[bar/mybar]
width = 100%
height = 24pt
radius = 6
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3pt
border-size = 4pt
border-color = #00000000
padding-left = 0
padding-right = 1
module-margin = 1
separator = |
separator-foreground = ${colors.disabled}
font-0 = monospace;2
modules-left = brand version device_id ping
modules-right = wlan eth date

[module/brand]
type = custom/text
content = VHH Workstation

[module/device_id]
type = custom/script
exec = echo -n 'ID: ...' && cat /etc/px-device-identity/device.yml | grep -w -m1 'id:' | cut -d' ' -f2 | rev | cut -c 1-8 | rev

[module/version]
type = custom/script
exec = echo -n 'v' && cat $(dirname $(dirname $(realpath $(which vhh-workstation))))/share/applications/vhh-workstation.desktop | grep 'Exec' | awk -F'-' '{print $(NF-1)}' | cut -d'/' -f1

[module/ping]
type = custom/script
exec = ping --timeout=1 vhh-server.ones-now.com > /dev/null 2>&1 && ping --timeout=1 vhh-server.ones-now.com | grep 'time=' | cut -d'=' -f4 | awk '{print $1 'ms'}' || echo 'CONNECTION DOWN'

[module/date]
type = internal/date
interval = 1
date = %H:%M
date-alt = %Y-%m-%d %H:%M:%S
label = %date%
label-foreground = ${colors.primary}

[network-base]
type = internal/network
interval = 5
format-connected = <label-connected>
format-disconnected = <label-disconnected>
label-disconnected = %{F#F0C674}%ifname%%{F#707880} disconnected

[module/eth]
inherit = network-base
interface-type = wired
label-connected = %{F#F0C674}%ifname%%{F-} %local_ip%

[module/pulseaudio]
type = internal/pulseaudio
"))))))))
   (home-page "https://pantherx.org")
   (synopsis "XDG autostart entry for vhh-workstation")
   (description "Adds a desktop entry to the ${XDG_CONFIG_DIRS}/autostart to run
vhh-workstation on openbox session start")
   (license license:expat)))

(define-public vhh-workstation-autostart-wayland-script
  (package
   (name "vhh-workstation-autostart-script")
   (version "0.0.4")
   (source #f)
   (build-system trivial-build-system)
   (inputs `(("coreutils" ,coreutils)
             ("vhh-workstation" ,vhh-workstation)))
   (propagated-inputs `(("python" ,python)
                        ("python-pyxdg" ,python-pyxdg)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out           (assoc-ref %outputs "out"))
               (autostart-dir (string-append out "/etc/xdg/autostart"))
               (executable    (string-append (assoc-ref %build-inputs "vhh-workstation")
                                             "/bin/vhh-workstation"))
               (desktop-entry (string-append autostart-dir "/vhh-workstation-start.desktop")))
          (setenv "PATH" 
		  (string-append (string-append (assoc-ref %build-inputs "coreutils")  "/bin:")
				 (getenv "PATH")))
          
          ;; Waybar config
          (let* ((rc-dir  (string-append out "/etc/xdg/waybar"))
                 (rc-file (string-append rc-dir "/config.jsonc")))
            (mkdir-p (string-append out "/etc/xdg/waybar"))
            (call-with-output-file (string-append out "/etc/xdg/waybar/config.jsonc")
              (lambda (port)
                (format port (string-append
			      "{\n"
			      "    \"layer\": \"top\",\n"
			      "    \"modules-left\": [\"sway/workspaces\", \"sway/mode\"],\n"
			      "    \"modules-center\": [\"sway/window\"],\n"
			      "    \"modules-right\": [\"battery\", \"clock\"],\n"
			      "    \"sway/window\": {\n"
			      "        \"max-length\": 50\n"
			      "    },\n"
			      "    \"battery\": {\n"
			      "        \"format\": \"{capacity}% {icon}\",\n"
			      "        \"format-icons\": [\"\", \"\", \"\", \"\", \"\"]\n"
			      "    },\n"
			      "    \"clock\": {\n"
			      "        \"format-alt\": \"{:%a, %d. %b  %H:%M}\"\n"
			      "    }\n"
			      "}")))))))))
   (home-page "https://pantherx.org")
   (synopsis "XDG autostart entry for vhh-workstation")
   (description "Adds a desktop entry to the ${XDG_CONFIG_DIRS}/autostart to run
vhh-workstation on openbox session start")
   (license license:expat)))