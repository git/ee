(define-module (softmax packages vhh-terminal)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages kde-utils)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages package-management)
  #:use-module (px packages device)
  #:use-module (px packages desktop-tools)
  #:use-module (px packages python-xyz)
  #:use-module (softmax packages alerts)
  #:use-module (softmax packages matrix)
  #:use-module (softmax packages health)
  #:use-module (softmax packages crates)
  #:use-module (srfi srfi-1)
  #:use-module ((guix licenses) #:prefix license:))

(define-public vhh-bluetooth-terminal-assets
  (package
   (name "vhh-bluetooth-terminal-assets")
   (version "3.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
     (sha256
      (base32 "0yma7nsirm0g2wyfr5didwfamhv29q2j67r6chzr4b826ifipmvp"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let ((tar (assoc-ref %build-inputs "tar"))
              (gzip (assoc-ref %build-inputs "gzip"))
              (src (assoc-ref %build-inputs "source"))
              (out-dir (string-append %output "/etc/vhh")))
          (mkdir-p out-dir)
          (setenv "PATH" (string-append gzip "/bin"))
          (invoke (string-append tar "/bin/tar") "xvf" src "-C" out-dir "--strip-components=1")
          #t))))
   (native-inputs
    `(("tar" ,tar)
      ("gzip" ,gzip)))
   (propagated-inputs (list polybar
                            kdialog
                            svkbd
                            bcms
                            px-terminal-launcher
                            matrix-client-call-auto-accept-v2
                            vhh-bluetooth-terminal-alerts
                            vhh-bluetooth-terminal-assigned-user
                            px-device-identity
                            px-device-identity-service
                            libnotify
                            dunst
                            px-device-runner
                            nss-certs
                            glibc-locales
                            neovim
                            sox
                            state-massage
                            usb-bpm-exporter
                            bpms
                            ones-system-stats))
   (home-page "https://www.softmax.co.th")
   (synopsis "Contains assets for vhh-bluetooth-terminal")
   (description "Configuration, scripts and other assets that power
VHH Bluetooth Terminal")
   (license license:expat)))