;;; Copyright © 2022 Franz Geffke <franz@pantherx.org> | PantherX.DEV

(define-module (softmax packages alerts)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages gnome) ;;libnotify
  #:use-module (gnu packages hardware)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages security-token)
  #:use-module (gnu packages admin)
  #:use-module (px packages python-xyz)
  #:use-module (px packages device)
  #:use-module (px packages tpm)
  #:use-module (softmax packages health)
  #:use-module ((guix licenses) #:prefix license:))


(define-public vhh-bluetooth-terminal-assigned-user
  (package
   (name "vhh-bluetooth-terminal-assigned-user")
   (version "0.0.16")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
     (sha256 (base32 "0ac1id8jnr3ra6yhqdp4ykpqpgl8xcsgig4kzwf5vswlpj5vldgj"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f
      #:phases 
      (modify-phases
       %standard-phases
       (add-after 'install 'wrap-for-openssl-tss2-conf
		  (lambda* (#:key outputs #:allow-other-keys)
		    (let ((out (assoc-ref outputs "out"))
			  (openssl (assoc-ref %build-inputs "openssl"))
			  (tpm2-tss (assoc-ref %build-inputs "tpm2-tss"))
			  (tpm2-tss-engine (assoc-ref %build-inputs "tpm2-tss-engine")))
		      (wrap-program (string-append out "/bin/vhh-bluetooth-terminal-assigned-user")
				    `("OPENSSL_CONF" ":" prefix
				      (,(string-append tpm2-tss-engine
						       "/etc/openssl-tss2.conf")))
				    `("PATH" ":" prefix
				      (,(string-append tpm2-tss-engine "/bin/") 
				       ,(string-append openssl "/bin/")))
				    `("TPM2TSSENGINE_TCTI" ":" prefix
				      (,(string-append tpm2-tss
						       "/lib/libtss2-tcti-device.so:/dev/tpm0")))
				    `("TPM2TOOLS_TCTI" ":" prefix
				      (,(string-append tpm2-tss
						       "/lib/libtss2-tcti-device.so:/dev/tpm0"))))
		      #t)))
       (delete 'sanity-check))))
   (inputs `(("openssl" ,openssl-1.1)
             ("tpm2-tss" ,tpm2-tss-openssl-1.1)
             ("tpm2-tss-engine" ,tpm2-tss-engine)
             ("libnotify" ,libnotify)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (propagated-inputs `(("px-device-identity" ,px-device-identity)
                        ("python-requests" ,python-requests)
                        ("px-python-shared" ,px-python-shared)))
   (home-page "https://www.softmax.co.th")
   (synopsis "Queries information about user assigned to the device.")
   (description "The service regularly polls VHH server for the user assigned
to the device, and updates a local config based on the result.")
   (license license:expat)))

(define-public vhh-bluetooth-terminal-alerts
  (package
   (name "vhh-bluetooth-terminal-alerts")
   (version "0.1.5")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
     (sha256 (base32 "1qxlgnqd2023535x18spbgd8lh7m5dsw0aiqmhcqw0b9iczci82s"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f
      #:phases 
      (modify-phases
       %standard-phases
       (add-after 'install 'wrap-for-openssl-tss2-conf
		  (lambda* (#:key outputs #:allow-other-keys)
		    (let ((out (assoc-ref outputs "out"))
			  (openssl (assoc-ref %build-inputs "openssl"))
			  (tpm2-tss (assoc-ref %build-inputs "tpm2-tss"))
			  (tpm2-tss-engine (assoc-ref %build-inputs "tpm2-tss-engine")))
		      (wrap-program (string-append out "/bin/vhh-bluetooth-terminal-alerts")
				    `("OPENSSL_CONF" ":" prefix
				      (,(string-append tpm2-tss-engine
						       "/etc/openssl-tss2.conf")))
				    `("PATH" ":" prefix
				      (,(string-append tpm2-tss-engine "/bin/") 
				       ,(string-append openssl "/bin/")))
				    `("TPM2TSSENGINE_TCTI" ":" prefix
				      (,(string-append tpm2-tss
						       "/lib/libtss2-tcti-device.so:/dev/tpm0")))
				    `("TPM2TOOLS_TCTI" ":" prefix
				      (,(string-append tpm2-tss
						       "/lib/libtss2-tcti-device.so:/dev/tpm0"))))
		      #t)))
       (delete 'sanity-check))))
   (inputs `(("openssl" ,openssl-1.1)
             ("tpm2-tss" ,tpm2-tss-openssl-1.1)
             ("tpm2-tss-engine" ,tpm2-tss-engine)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (propagated-inputs `(("px-device-identity" ,px-device-identity)
                        ("python-requests" ,python-requests)
                        ("px-python-shared" ,px-python-shared)
                        ("libnotify" ,libnotify)))
   (home-page "https://www.softmax.co.th")
   (synopsis "Alerts device user about alerts triggered on the server.")
   (description "The service regularly polls VHH server for alerts related 
to the user assigned to the device and triggers on-screen alerts.")
   (license license:expat)))

(define-public bpms
  (package
   (name "bpms")
   (version "0.0.6")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
     (sha256 (base32 "1raykv1birr6yw1q61916kh5qk4d2w0iji1kxyn9v4ss8gkcx0xs"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f
      #:phases 
      (modify-phases
       %standard-phases
       (add-after 'install 'wrap-for-openssl-tss2-conf
		  (lambda* (#:key outputs #:allow-other-keys)
		    (let ((out (assoc-ref outputs "out"))
			  (openssl (assoc-ref %build-inputs "openssl"))
			  (tpm2-tss (assoc-ref %build-inputs "tpm2-tss"))
			  (tpm2-tss-engine (assoc-ref %build-inputs "tpm2-tss-engine")))
		      (wrap-program (string-append out "/bin/bpms-daemon")
				    `("OPENSSL_CONF" ":" prefix
				      (,(string-append tpm2-tss-engine
						       "/etc/openssl-tss2.conf")))
				    `("PATH" ":" prefix
				      (,(string-append tpm2-tss-engine "/bin/") 
				       ,(string-append openssl "/bin/")))
				    `("TPM2TSSENGINE_TCTI" ":" prefix
				      (,(string-append tpm2-tss
						       "/lib/libtss2-tcti-device.so:/dev/tpm0")))
				    `("TPM2TOOLS_TCTI" ":" prefix
				      (,(string-append tpm2-tss
						       "/lib/libtss2-tcti-device.so:/dev/tpm0"))))
		      #t)))
       (delete 'sanity-check))))
   (inputs `(("openssl" ,openssl-1.1)
             ("tpm2-tss" ,tpm2-tss-openssl-1.1)
             ("tpm2-tss-engine" ,tpm2-tss-engine)
             ("libnotify" ,libnotify)))
   (native-inputs `(("pkg-config" ,pkg-config)))
   (propagated-inputs `(("px-device-identity" ,px-device-identity)
                        ("px-python-shared" ,px-python-shared)
                        ("python-requests" ,python-requests)
                        ("python-appdirs" ,python-appdirs)
                        ("python-pyudev" ,python-pyudev)
                        ("usb-bpm-exporter" ,usb-bpm-exporter)
                        ("bcms" ,bcms)
                        ("python-sentry-sdk" ,python-sentry-sdk-2)))
   (home-page "https://www.softmax.co.th")
   (synopsis "Retrieve blood pressure data from USB BPM")
   (description "Retrieve blood pressure data from USB BPM and submit to IDP.")
   (license license:expat)))