(define-module (softmax packages cpp)
  #:use-module (guix build-system qt)
  #:use-module (guix download)
  #:use-module (guix packages)

  #:use-module (guix gexp)
  #:use-module (ice-9 match)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages check)
  #:use-module (srfi srfi-1)
  #:use-module ((guix licenses) #:prefix license:))

(define-public ones-oidc-cpp
  (package
    (name "ones-oidc-cpp")
    (version "0.2.8")
    (source
    (origin
      (method url-fetch)
      (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
      (sha256
       (base32 "070qlx4py73gp0lp92ykd3284qdbcjrp4i4hb0q4qgaqm8df3cnn"))))
    (build-system qt-build-system)
    (arguments
     `(#:tests? #f))
    (inputs (list qtbase-5
                  openssl
                  qrcodegen-cpp
                  googletest))
    (native-inputs (list pkg-config))
   (home-page "https://www.softmax.co.th")
   (synopsis "CIBA, QR and Device Authentication")
   (description "CIBA and QR flow and device authentication library")
    (license license:expat)))

(define-public ones-stats-cpp
  (package
    (name "ones-stats-cpp")
    (version "0.1.6")
    (source
    (origin
      (method url-fetch)
      (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
      (sha256
       (base32 "11r8sb66f2k2rdirr2b8mwbrsd6axvfg00v7hgadb4kbk47c8n26"))))
    (build-system qt-build-system)
    (arguments
     `(#:tests? #f))
    (inputs (list qtbase-5
                  openssl
                  qrcodegen-cpp
                  ones-oidc-cpp))
    (native-inputs (list pkg-config))
   (home-page "https://www.softmax.co.th")
   (synopsis "Library to access ONES Stats API")
   (description "Retrieve, and submit data to ONES stats API.")
    (license license:expat)))

(define-public ones-documents-cpp
  (package
    (name "ones-documents-cpp")
    (version "0.1.1")
    (source
    (origin
      (method url-fetch)
      (uri (string-append "https://source.pantherx.org/" name "_v" version ".tgz"))
      (sha256
       (base32 "0y4mfhmzrfcqjjbnr7qkmq040114wlfyf2s43qqn55kpfk52343i"))))
    (build-system qt-build-system)
    (arguments
     `(#:tests? #f))
    (inputs (list qtbase-5
                  openssl
                  qrcodegen-cpp
                  ones-oidc-cpp))
    (native-inputs (list pkg-config))
   (home-page "https://www.softmax.co.th")
   (synopsis "Library to access ONES Stats API")
   (description "Retrieve, and submit data to ONES stats API.")
    (license license:expat)))

