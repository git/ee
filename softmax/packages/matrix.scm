(define-module (softmax packages matrix)
  #:use-module (guix packages)
  #:use-module (px packages matrix-client)
  #:use-module (softmax packages cpp)
  #:use-module (srfi srfi-1))

(define-public matrix-client-library-with-ciba-v2
  (package
    (inherit matrix-client-library-with-ciba)
    (name "matrix-client-library-with-ciba")
    (version "0.2.0")
    (inputs `(("ones-oidc-cpp" ,ones-oidc-cpp)
              ,@(package-inputs matrix-client-library-with-ciba)))))

(define-public matrix-client-gui-library-v2
  (package
    (inherit matrix-client-gui-library)
    (name "matrix-client-gui-library")
    (inputs `(("px-matrix-client-library-with-ciba" ,matrix-client-library-with-ciba-v2)
              ,@(fold alist-delete
                      (package-inputs matrix-client-gui-library)
                      '("matrix-client-library"))))))

(define-public matrix-client-v2
  (package
    (inherit matrix-client)
    (name "matrix-client-v2")
    (inputs `(("matrix-client-gui-library" ,matrix-client-gui-library-v2)
              ,@(fold alist-delete
                      (package-inputs matrix-client-gui-library)
                      '("matrix-client-gui-library"))))))

(define-public matrix-client-call-auto-accept-v2
  (package
    (inherit matrix-client-call-auto-accept)
    (name "matrix-client-call-auto-accept-v2")
    (inputs `(("matrix-client-gui-library" ,matrix-client-gui-library-v2)
              ,@(fold alist-delete
                      (package-inputs matrix-client-gui-library)
                      '("matrix-client-gui-library"))))))