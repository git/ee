# Softmax Enterprise Package Repository

This repository contains the application packages and system configuration for:

- VHH Workstation
- OnesMAX KYC

## Stable System Configuration

Below is the current **stable** configuration that should be applied to all running and future systems, that use matching hardware: VHH Workstation on Thinkstation 625 and OnesMAX KYC on Onlogic.

### Workstation OS

To install a new workstation on NUC 11, run the following command on a recent PantherX OS installation image:

```bash
px-install --config 67b026af46
```

To migrate legacy Workstation's, run this as root:

```bash
curl -sSL https://temp.pantherx.org/ones-now_migrate.sh | bash
```

#### Channels

Thanks to upstream changes, channels are now baked-in to the system configuration.

#### Configuration

File at `/etc/system.scm`

```scheme
;; PantherX OS enterprise system configuration file.
;; Use this configuration file for VHH Workstation on Thinkstation.
;;
;; Version: 2.0.2

(use-modules (gnu)
         (softmax system))

(softmax-workstation-os
 (operating-system
  (host-name "vhh-workstation")
  (timezone "Asia/Bangkok")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
                 (bootloader grub-efi-bootloader)
                  (target "/boot/efi")))

  (file-systems (append
                 (list
                  (file-system
                    (device (file-system-label "my-root"))
                    (mount-point "/")
                    (type "ext4"))
                  (file-system
                    (device "/dev/nvme0n1p1")
                    (mount-point "/boot/efi")
                    (type "vfat")))
                 %base-file-systems))))
```

### OnesMAX KYC OS

To install a new KYC on Onlogic, run the following command on a recent PantherX OS installation image:

```bash
px-install --config 38c80fb6cf
```

To migrate legacy KYC's, run this as root:

```bash
curl -sSL https://temp.pantherx.org/ones-now_migrate.sh | bash
```

#### Channels

Thanks to upstream changes, channels are now baked-in to the system configuration.

#### Configuration

File at `/etc/system.scm`

```scheme
;; PantherX OS enterprise system configuration file.
;; Use this configuration file for OnesMAX on Onlogic.
;;
;; Version: 2.0.2

(use-modules (gnu)
         (softmax system))

(softmax-kyc-os
 (operating-system
  (host-name "onesmax-kyc")
  (timezone "Asia/Bangkok")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
                 (bootloader grub-efi-bootloader)
                  (target "/boot/efi")))

  (file-systems (append
                 (list
                  (file-system
                    (device (file-system-label "my-root"))
                    (mount-point "/")
                    (type "ext4"))
                  (file-system
                    (device "/dev/sda1")
                    (mount-point "/boot/efi")
                    (type "vfat")))
                 %base-file-systems))))
```

## Testing

### Operating System

```bash
./pre-inst-env guix system vm ../ee-softmax-examples/kyc-os.scm
```

### Applications

```bash
guix time-machine --channels=channels.scm -- build kyc
```

## Known System Configs

| Date                             | ID         | Device           | Type                      | Application     | Branch     | Tested    |
| -------------------------------- | ---------- | ---------------- | ------------------------- | --------------- | ---------- | --------- |
| Thu Jul 7 09:31:03 AM WEST 2022  | b8308d7419 | Thinkstation 625 | OTHER (1)                 | VHH Workstation | stable     | FG        |
| Thu Mar 17 03:51:15 PM WET 2022  | 3ca7159514 | Onlogic          | REGISTRATION_TERMINAL (1) | KYC             | stable     | FG; 17.03 |
| Fri Jan 20 04:16:14 PM WET 2023  | d7ca01e0b1 | NUC 11th Gen     | DESKTOP (1)               | VHH Workstation | production | FG        |
| Fri Jan 20 04:17:08 PM WET 2023  | eb672f1b0e | Onlogic          | REGISTRATION_TERMINAL (1) | OnesMAX         | production | FG        |
| Thu Sep 28 11:33:42 AM WEST 2023 | 0f71132718 | QEMU             | DESKTOP (1)               | VHH Workstation | master     |           |
| Thu Sep 28 09:08:18 PM WEST 2023 | 3749a0aa00 | QEMU             | DESKTOP                   | VHH Workstation | master     |           |
| Sun Oct 1 10:03:18 AM WEST 2023  | 62db2146e9 | NUC 11th Gen     | DESKTOP                   | VHH Workstation | master     |           |
| Tue Oct 31 09:43:46 PM WET 2023  | 6cf6992a8e | NUC 11th Gen     | DESKTOP                   | VHH Workstation | master     |           |
| Wed Nov 1 08:36:25 PM WET 2023   | 67b026af46 | NUC 11th Gen     | DESKTOP (2)               | VHH Workstation | master     |           |
| Wed Nov 1 09:04:19 PM WET 2023   | 38c80fb6cf | Onlogic          | REGISTRATION_TERMINAL (2) | KYC             | master     |           |
| Thu Nov 2 09:55:06 AM WET 2023   | 9338c16a3e | QEMU             | REGISTRATION_TERMINAL (2) | KYC             | master     |           |

- (1) Requires older image; for ex https://temp.pantherx.org/pantherx-1.4.0-1.9fe5b49-image.iso.tar.gz
- (2) NEW: Dedicated channels at https://channels.ones-now.com